package ru.thstdio17.goodhobby;

import android.app.Application;

import ru.thstdio17.goodhobby.dagger.AppComponent;
import ru.thstdio17.goodhobby.dagger.BdModule;
import ru.thstdio17.goodhobby.dagger.DaggerAppComponent;
import ru.thstdio17.goodhobby.room.RoomHelper;
import ru.thstdio17.goodhobby.sqlite.SqliteBD;

public class HobbyApp extends Application {

    private static RoomHelper room;
    AppComponent component;
    @Override
    public void onCreate() {
        super.onCreate();
        SqliteBD.getInstance(getApplicationContext());
        room=new RoomHelper(getApplicationContext());
        component = DaggerAppComponent.builder().bdModule(new BdModule(getApplicationContext())).build();

    }

    public static RoomHelper getBd(){
        return room;
    }
}
