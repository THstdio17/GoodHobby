package ru.thstdio17.goodhobby.room.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "finish")
public class FinishEntityRoom {
    @PrimaryKey
    int id;
    long finish;

}
