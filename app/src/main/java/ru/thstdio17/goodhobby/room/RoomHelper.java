package ru.thstdio17.goodhobby.room;

import android.arch.persistence.room.Room;
import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import ru.thstdio17.goodhobby.hobby.Hobby;
import ru.thstdio17.goodhobby.hobby.HobbyScheduler;
import ru.thstdio17.goodhobby.hobby.LogItem;
import ru.thstdio17.goodhobby.nottoall.LogsUnit;
import ru.thstdio17.goodhobby.room.entity.HobbyEntityRoom;

public class RoomHelper implements IBd {
    AppDB db;

    public RoomHelper(Context context) {
        db = Room.databaseBuilder(context, AppDB.class, "hobby.db").build();
    }


    @Override
    public void setHobby(Hobby hobby) {
        HobbyEntityRoom hobbyRoom = new HobbyEntityRoom();
        hobbyRoom.fromHobby(hobby);
        db.hobbyDao().insert(hobbyRoom);
    }

    @Override
    public Hobby getHobbyForId(int id) {
        return db.hobbyDao().getHobbyById(id).toHobby();
    }

    @Override
    public Hobby getHobbyForPosition(int position) {
        return null;
    }

    @Override
    public List<Hobby> getListHobby() {
        return null;
    }

    @Override
    public void setHobbyResult(int id, int progress) {

    }

    @Override
    public void setHobbyResult(int id, long start, int progress) {

    }

    @Override
    public void startHobby(Hobby hobby) {

    }

    @Override
    public int summProgressHobby(int id) {
        return 0;
    }

    @Override
    public void updateHobby(Hobby hobby) {

    }

    @Override
    public void finishHobby(Hobby hobby) {

    }

    @Override
    public long getHobbyFinish(int id) {
        return 0;
    }

    @Override
    public void setSheduler(HobbyScheduler shelder) {

    }

    @Override
    public ArrayList<Integer> getIdsSheduler() {
        return null;
    }

    @Override
    public List<HobbyScheduler> getShedulerList() {
        return null;
    }

    @Override
    public HobbyScheduler getShedulerList(int id) {
        return null;
    }

    @Override
    public void deleteShuduler(int id) {

    }

    @Override
    public void deleteHobbyFull(int id) {

    }

    @Override
    public List<LogItem> getLogHobby(int id) {
        return null;
    }

    @Override
    public void writeLog(String tag, String txt) {

    }

    @Override
    public List<LogsUnit> readLog(String tag) {
        return null;
    }

    @Override
    public List<LogsUnit> readLog() {
        return null;
    }

    @Override
    public void delLog() {

    }
}
