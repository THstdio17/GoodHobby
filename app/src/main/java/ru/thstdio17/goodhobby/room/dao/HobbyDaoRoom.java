package ru.thstdio17.goodhobby.room.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import ru.thstdio17.goodhobby.room.entity.HobbyEntityRoom;

@Dao
public interface HobbyDaoRoom {
    @Query("SELECT * FROM hobby")
    List<HobbyEntityRoom> getAll();

    @Insert
    void insert(HobbyEntityRoom hobby);

    @Update
    void update(HobbyEntityRoom hobby);

    @Delete
    void delete(HobbyEntityRoom hobby);

    @Query("SELECT * FROM hobby where id=:id")
    HobbyEntityRoom getHobbyById(int id);
}
