package ru.thstdio17.goodhobby.room;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import ru.thstdio17.goodhobby.room.dao.HobbyDaoRoom;
import ru.thstdio17.goodhobby.room.entity.HobbyEntityRoom;

@Database(entities = {HobbyEntityRoom.class}, version = 2)
public abstract class AppDB extends RoomDatabase {
    public abstract HobbyDaoRoom hobbyDao();

}
