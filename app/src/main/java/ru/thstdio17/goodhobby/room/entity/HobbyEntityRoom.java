package ru.thstdio17.goodhobby.room.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import ru.thstdio17.goodhobby.hobby.Hobby;

@Entity(tableName = "hobby")
public class HobbyEntityRoom {
    @PrimaryKey(autoGenerate = true)
    public int id;
    public int position;
    public String name;
    public String description;
    public long start;
    public int status;
    public int icon_id;
    public int one_day;

    public void fromHobby(Hobby hobby){
        this.id=hobby.getId();
        this.position=hobby.getPosition();
        this.name=hobby.getName();
        this.description=hobby.getDescription();
        this.start=hobby.getStart();
        this.status=hobby.getStatus();
        this.icon_id=hobby.getIconId();
        this.one_day=hobby.getOneDay();
    }
    public Hobby toHobby(){
        Hobby hobby = new Hobby(id, position, name);
        hobby.setDescription(description);
        hobby.setStart(start);
        hobby.setStatus(status);
        hobby.setOneDay(one_day == 1);
        hobby.setIconId(icon_id);
        return hobby;
    }
}
