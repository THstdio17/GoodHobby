package ru.thstdio17.goodhobby.room;

import java.util.ArrayList;
import java.util.List;

import ru.thstdio17.goodhobby.hobby.Hobby;
import ru.thstdio17.goodhobby.hobby.HobbyScheduler;
import ru.thstdio17.goodhobby.hobby.LogItem;
import ru.thstdio17.goodhobby.nottoall.LogsUnit;

public interface IBd {
    void setHobby(Hobby hobby);

    Hobby getHobbyForId(int id);

    Hobby getHobbyForPosition(int position);

    List<Hobby> getListHobby();

    void setHobbyResult(int id, int progress);

    void setHobbyResult(int id, long start, int progress);

    void startHobby(Hobby hobby);

    int summProgressHobby(int id);

    void updateHobby(Hobby hobby);

    void finishHobby(Hobby hobby);

    long getHobbyFinish(int id);

    void setSheduler(HobbyScheduler shelder);

    ArrayList<Integer> getIdsSheduler();

    List<HobbyScheduler> getShedulerList();

    HobbyScheduler getShedulerList(int id);

    void deleteShuduler(int id);

    void deleteHobbyFull(int id);

    List<LogItem> getLogHobby(int id);

    void writeLog(String tag, String txt);

    List<LogsUnit> readLog(String tag);

    List<LogsUnit> readLog();

    void delLog();

}
