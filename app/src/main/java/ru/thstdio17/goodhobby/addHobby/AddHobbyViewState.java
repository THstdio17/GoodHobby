package ru.thstdio17.goodhobby.addHobby;

public interface AddHobbyViewState {
    void setHobbyToView(String name, int iconId, String description);
    void setShelderToView(int shelderType);


    void dayCheckAll(boolean b);

    void dayCheck(int i);

    boolean getStateSwOld();

    long getOldTime();

    int getOldCount();
}
