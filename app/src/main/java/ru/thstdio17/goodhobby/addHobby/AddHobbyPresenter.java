package ru.thstdio17.goodhobby.addHobby;

import ru.thstdio17.goodhobby.hobby.DateLab;
import ru.thstdio17.goodhobby.hobby.Hobby;
import ru.thstdio17.goodhobby.hobby.HobbyScheduler;
import ru.thstdio17.goodhobby.sqlite.SqliteBD;

public class AddHobbyPresenter {
    private final SqliteBD bs;
    private AddHobbyViewState stateView;

    private Hobby hobby;
    HobbyScheduler shelder;
    int shelderType = 0;
    boolean isDayCheck[] = {true, true, true, true, true, true, true};

    public AddHobbyPresenter() {
        bs = SqliteBD.getInstance();
    }

    public void setStateView(AddHobbyViewState stateView) {
        this.stateView = stateView;
    }

    public void initHobby(int id) {
        if (id > 0) {
            hobby = bs.getHobbyForId(id);
            shelder = bs.getShedulerList(id);
        }
    }

    public void setShelder(HobbyScheduler shelder) {
        this.shelder = shelder;
    }

    public void loadHobbyToDisplay() {
        if (hobby != null)
            stateView.setHobbyToView(hobby.getName(), hobby.getIconId(), hobby.getDescription());

        if (shelder != null) {

            shelderType = shelder.getType();
            stateView.setShelderToView(shelderType);
            switch (shelderType) {
                case 0:
                    stateView.dayCheckAll(true);
                    break;
                case 1:
                case 2:
                    stateView.dayCheck(shelder.getDays(0) - 1);
                    break;
                case 3:
                    for (int i = 0; i < shelder.size(); i++) stateView.dayCheck(shelder.getDays(i));
            }
        }
    }

    public void save(String strTitle, String strDescription, int iconId, boolean isDialogDay) {
        if (hobby != null) {
            hobby.setName(strTitle);
            hobby.setDescription(strDescription);
            hobby.setIconId(iconId);
            if (isDialogDay) hobby.setOneDay(true);
            update();
            return;
        }
        int position = bs.getMaxPosition() + 1;
        hobby = new Hobby(0, position, strTitle).setDescription(strDescription);
        hobby.setOneDay(true);
        hobby.setIconId(iconId);
        bs.setHobby(hobby);
        hobby = bs.getHobbyForPosition(position);

        createShelder();
        bs.setSheduler(shelder);
        updateLastHobby();
    }

    private void update() {
        bs.updateHobby(hobby);
        bs.deleteShuduler(hobby.getId());
        createShelder();
        bs.setSheduler(shelder);
        updateLastHobby();
    }

    private void createShelder() {
        if (shelder == null) {
            shelder = new HobbyScheduler();
        }
        shelder.setHobbyId(hobby.getId());
        shelder.clearDays();
        int n = shelderType == 3 ? 0 : 1;

        for (int i = 0; i < 7; i++) {
            if (isDayCheck[i]) shelder.addDays(i + n);
        }
        if (shelderType == 3 && shelder.size() == 7) shelderType = 0;
        shelder.setType(shelderType);

    }

    private void updateLastHobby() {
        long oldTime = stateView.getOldTime();
        if (stateView.getStateSwOld()) {
            long delta = DateLab.now() - oldTime;
            int days = (int) (delta / DateLab.HMs(24));
            int intOldCount = stateView.getOldCount();
            int stepDay = 1;
            int avgOldCount = intOldCount / days;
            if (avgOldCount == 0) {
                avgOldCount = 1;
                stepDay = days / intOldCount;
            }
            hobby.setStart(oldTime);
            hobby.setStatus(0);
            bs.updateHobby(hobby);
            int tempCount = 0;
            for (int i = 0; i < days; i += stepDay) {
                bs.setHobbyResult(hobby.getId(), oldTime + i * DateLab.HMs(24), avgOldCount);
                tempCount += avgOldCount;
            }
            bs.setHobbyResult(hobby.getId(), intOldCount - tempCount);
        }
    }
}
