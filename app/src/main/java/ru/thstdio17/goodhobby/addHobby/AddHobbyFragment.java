package ru.thstdio17.goodhobby.addHobby;

import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.SwitchCompat;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Date;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import ru.thstdio17.goodhobby.R;
import ru.thstdio17.goodhobby.hobby.DateLab;


/**
 * Created by shcherbakov on 09.08.2017.
 */

public class AddHobbyFragment extends Fragment implements AddHobbyViewState, View.OnClickListener, AddHobbyActivity.Calback, CompoundButton.OnCheckedChangeListener {
    private static String EXTRA_ID_HOBBY = "id_hobby";
    private String EXTRA_SHEDULER = "shelder";

    AddHobbyPresenter presenter = new AddHobbyPresenter();


    boolean isDialogDay = false;
    private int iconId = 0;


    String[] dayWeekBrif;

    TypedArray icons;

    @BindView(R.id.editTextTitle)
    EditText title;
    @BindView(R.id.editTextDescription)
    EditText description;
    @BindView(R.id.imageIcon)
    ImageView icon;
    @BindView(R.id.spinner)
    Spinner typeDay;
    @BindView(R.id.scrollImage)
    ScrollView scroll;

    @BindViews({R.id.textDays0, R.id.textDays1, R.id.textDays2, R.id.textDays3, R.id.textDays4, R.id.textDays5, R.id.textDays6})
    List<TextView> days;
    @BindViews({R.id.imageDays0, R.id.imageDays1, R.id.imageDays2, R.id.imageDays3, R.id.imageDays4, R.id.imageDays5, R.id.imageDays6})
    List<ImageView> imageDays;
    @BindViews({R.id.imageIcon0, R.id.imageIcon1, R.id.imageIcon2, R.id.imageIcon3, R.id.imageIcon4, R.id.imageIcon5, R.id.imageIcon6,
            R.id.imageIcon7, R.id.imageIcon8, R.id.imageIcon9, R.id.imageIcon10, R.id.imageIcon11})
    List<ImageView> imageIcon;

    @BindView(R.id.switchOld)
    SwitchCompat swOld;
    @BindView(R.id.textViewData)
    TextView txtDateOld;
    @BindView(R.id.editTextCountOld)
    EditText countOld;
    @BindView(R.id.linearLayoutNotNull)
    LinearLayout llNonNull;
    @BindView(R.id.buttonDateOld)
    Button btnDateOld;
    @BindView(R.id.buttonDataOk)
    Button btnDateOk;
    @BindView(R.id.scrollDataPicker)
    RelativeLayout scrollData;
    @BindView(R.id.datePicker)
    DatePicker dataPicer;
    Date oldDate;

    public static AddHobbyFragment newInstance(int idHobby) {
        Bundle args = new Bundle();
        args.putInt(EXTRA_ID_HOBBY, idHobby);
        AddHobbyFragment fragment = new AddHobbyFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        presenter.setStateView(this);
        if (savedInstanceState != null) {
            presenter.setShelder(savedInstanceState.getParcelable(EXTRA_SHEDULER));
        }
        super.onCreate(savedInstanceState);
        assert getArguments() != null;
        int id = getArguments().getInt(EXTRA_ID_HOBBY);
        presenter.initHobby(id);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.m_add_hobby_fragment, container, false);
        ButterKnife.bind(this, v);
        initView();
        setListerns();
        presenter.loadHobbyToDisplay();
        return v;

    }

    private void initView() {
        icons = getResources().obtainTypedArray(R.array.icon_res);
        dayWeekBrif = getResources().getStringArray(R.array.day_week_brif);

        swOld.setOnCheckedChangeListener(this);
        btnDateOld.setOnClickListener(this);
        btnDateOk.setOnClickListener(this);
    }

    private void setListerns() {
        icon.setOnClickListener(this);
        for (int i = 0; i < imageIcon.size(); i++) {
            setIconToView(imageIcon.get(i), i);
            imageIcon.get(i).setOnClickListener(this);
        }
        iconId = 0;
        typeDay.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int pos,
                                       long id) {
                // TODO Auto-generated method stub
                if (presenter.shelderType != pos) {
                    presenter.shelderType = pos;
                    daysStatus(pos, true);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });
        Display display = Objects.requireNonNull(getActivity()).getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        for (int i = 0; i < 7; i++) {
            days.get(i).setOnClickListener(this);
            imageDays.get(i).setMinimumWidth(width / 10);
            imageDays.get(i).setMinimumHeight(width / 10);
        }
    }

    private void daysStatus(int pos, boolean isNew) {
        imageDays.get(0).setVisibility(View.VISIBLE);
        days.get(0).setVisibility(View.VISIBLE);

        switch (pos) {
            case 0:
                for (int i = 0; i < 7; i++) {
                    days.get(i).setText(dayWeekBrif[i]);
                }
                dayCheckAll(true);
                break;
            case 1:
                imageDays.get(0).setVisibility(View.GONE);
                days.get(0).setVisibility(View.GONE);
                for (int i = 0; i < 7; i++) {
                    days.get(i).setText(String.valueOf(i + 1));
                }
                dayCheckAll(false);
                if (isNew) dayCheck(1);
                break;
            case 2:
                for (int i = 0; i < 7; i++) {
                    days.get(i).setText(String.valueOf(i + 1));
                }
                dayCheckAll(false);
                if (isNew) dayCheck(2);
                break;
            case 3:
                for (int i = 0; i < 7; i++) {
                    days.get(i).setText(dayWeekBrif[i]);
                }
                dayCheckAll(false);

                break;
        }
    }

    private boolean save() {
        if (swOld.isChecked()) {
            if (countOld.getText().toString().equals("")) {
                String str = (String) getText(R.string.add_hobby_error_old_count);
                Toast toast = Toast.makeText(getContext(),
                        str, Toast.LENGTH_SHORT);
                toast.show();
                return false;
            }

        }
        String strDescription = description.getText().toString();
        String strTitle = title.getText().toString();
        presenter.save(strTitle, strDescription, iconId, isDialogDay);
        return true;
    }


    private void clickDay(int id) {
        if (presenter.shelderType == 1 || presenter.shelderType == 2) dayCheckAll(false);
        else if (presenter.shelderType == 0) {
            typeDay.setSelection(3);
            presenter.shelderType = 3;
        }
        dayCheck(id);
    }

    @Override
    public void dayCheck(int index) {
        if (presenter.isDayCheck[index]) {
            imageDays.get(index).setVisibility(View.INVISIBLE);
            days.get(index).setTextColor(Color.BLACK);
        } else {
            imageDays.get(index).setVisibility(View.VISIBLE);
            days.get(index).setTextColor(Color.WHITE);
        }
        presenter.isDayCheck[index] = !presenter.isDayCheck[index];
    }

    @Override
    public void dayCheckAll(boolean on) {
        for (int i = 0; i < 7; i++) {
            presenter.isDayCheck[i] = !on;
            dayCheck(i);
        }
    }

    private void animation(View v, int on) {
        Animation animation;
        switch (on) {
            case 0:
                v.setVisibility(View.VISIBLE);
                animation = AnimationUtils.loadAnimation(getContext(), R.anim.go_out);
                v.setAnimation(animation);
                v.animate();
                ((AddHobbyActivity) Objects.requireNonNull(getActivity())).fabVisble(false);
                break;
            case 1:
                animation = AnimationUtils.loadAnimation(getContext(), R.anim.go_on);
                v.setAnimation(animation);
                v.animate();
                ((AddHobbyActivity) Objects.requireNonNull(getActivity())).fabVisble(true);
                v.setVisibility(View.GONE);
        }

    }

    private void setIconToView(ImageView image, int idRes) {
        iconId = idRes;
        image.setImageResource(icons.getResourceId(idRes, 0));
        //  image.setImageResource(iconRes[idRes]);
    }

    @Override
    public void onClick(View view) {

        int id = view.getId();
        switch (id) {
            case R.id.textDays0:
                clickDay(0);
                break;
            case R.id.textDays1:
                clickDay(1);
                break;
            case R.id.textDays2:
                clickDay(2);
                break;
            case R.id.textDays3:
                clickDay(3);
                break;
            case R.id.textDays4:
                clickDay(4);
                break;
            case R.id.textDays5:
                clickDay(5);
                break;
            case R.id.textDays6:
                clickDay(6);
                break;
            case R.id.imageIcon:
                animation(scroll, 0);
                break;
            case R.id.imageIcon0:
                setIconToView(icon, 0);
                animation(scroll, 1);
                break;
            case R.id.imageIcon1:
                setIconToView(icon, 1);
                animation(scroll, 1);
                break;
            case R.id.imageIcon2:
                setIconToView(icon, 2);
                animation(scroll, 1);
                break;
            case R.id.imageIcon3:
                setIconToView(icon, 3);
                animation(scroll, 1);
                break;
            case R.id.imageIcon4:
                setIconToView(icon, 4);
                animation(scroll, 1);
                break;
            case R.id.imageIcon5:
                setIconToView(icon, 5);
                animation(scroll, 1);
                break;
            case R.id.imageIcon6:
                setIconToView(icon, 6);
                animation(scroll, 1);
                break;
            case R.id.imageIcon7:
                setIconToView(icon, 7);
                animation(scroll, 1);
                break;
            case R.id.imageIcon8:
                setIconToView(icon, 8);
                animation(scroll, 1);
                break;
            case R.id.imageIcon9:
                setIconToView(icon, 9);
                animation(scroll, 1);
                break;
            case R.id.imageIcon10:
                setIconToView(icon, 10);
                animation(scroll, 1);
                break;
            case R.id.imageIcon11:
                setIconToView(icon, 11);
                animation(scroll, 1);
                break;
            case R.id.buttonDateOld:
                animation(scrollData, 0);
                break;
            case R.id.buttonDataOk:
                setOldDate();
                break;

        }

    }

    private void setOldDate() {
        int y = dataPicer.getYear();
        int m = dataPicer.getMonth();
        int d = dataPicer.getDayOfMonth();
        oldDate = DateLab.getDateYMD(y, m, d);
        long t1 = DateLab.now();
        long t2 = oldDate.getTime();
        long delta = t1 - t2;
        if (delta > 0) animation(scrollData, 1);
        else {
            String str = (String) getText(R.string.add_hobby_error_time);
            Toast toast = Toast.makeText(getContext(),
                    str, Toast.LENGTH_SHORT);
            toast.show();
        }
        txtDateOld.setText(DateLab.parceDateplus(oldDate.getTime(), getResources().getStringArray(R.array.mounth)));
    }

    @Override
    public void fab() {
        if (save()) Objects.requireNonNull(getActivity()).finish();
    }

    @Override
    public void back() {
        if (scroll.getVisibility() == View.VISIBLE) setOldDate();
        else if (scrollData.getVisibility() == View.VISIBLE) animation(scrollData, 1);
        else {
            AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(getContext()));
            builder.setMessage(getResources().getText(R.string.add_hobby_is_save))
                    .setCancelable(true)
                    .setPositiveButton("Ok",
                            (dialog, id) -> {
                                save();
                                Objects.requireNonNull(getActivity()).finish();
                                dialog.cancel();
                            }).setNegativeButton(getString(R.string.cancel),
                    (dialog, id) -> {
                        Objects.requireNonNull(getActivity()).finish();
                        dialog.cancel();
                    });
            AlertDialog alert = builder.create();
            alert.show();
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putParcelable(EXTRA_SHEDULER, presenter.shelder);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        countOld.setFocusableInTouchMode(true);
        llNonNull.setVisibility(b ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setHobbyToView(String name, int iconId, String strDescription) {
        title.setText(name);
        setIconToView(icon, iconId);
        description.setText(strDescription);
    }

    @Override
    public void setShelderToView(int shelderType) {
        typeDay.setSelection(shelderType);
        daysStatus(shelderType, false);
    }

    @Override
    public boolean getStateSwOld() {
        return swOld.isChecked();
    }

    @Override
    public long getOldTime() {
        return oldDate.getTime();
    }

    @Override
    public int getOldCount() {
        return Integer.parseInt(countOld.getText().toString());
    }
}
