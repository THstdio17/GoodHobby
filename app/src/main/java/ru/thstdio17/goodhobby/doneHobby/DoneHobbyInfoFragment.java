package ru.thstdio17.goodhobby.doneHobby;

import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import ru.thstdio17.goodhobby.R;
import ru.thstdio17.goodhobby.addHobby.AddHobbyActivity;
import ru.thstdio17.goodhobby.hobby.DateLab;
import ru.thstdio17.goodhobby.hobby.Hobby;
import ru.thstdio17.goodhobby.hobby.HobbyScheduler;
import ru.thstdio17.goodhobby.sqlite.SqliteBD;

/**
 * Created by shcherbakov on 27.08.2017.
 */

public class DoneHobbyInfoFragment extends Fragment implements View.OnClickListener, DoneHobbyActivity.ToUdate {
    private static String EXTRA_ID_HOBBY = "id_hobby";

    SqliteBD bs;
    TypedArray icons;

    Hobby hobby;
    HobbyScheduler shelder;

    @BindView(R.id.editTextTitle)
    TextView title;
    @BindView(R.id.editTextDescription)
    TextView description;
    @BindViews({R.id.textDays0,R.id.textDays1,R.id.textDays2,R.id.textDays3,R.id.textDays4,R.id.textDays5,R.id.textDays6})
    List<TextView> days ;
    @BindViews({R.id.imageDays0,R.id.imageDays1,R.id.imageDays2,R.id.imageDays3,R.id.imageDays4,R.id.imageDays5,R.id.imageDays6})
    List<ImageView> imageDays ;
    @BindView(R.id.imageIcon)
    ImageView icon;
    @BindView(R.id.spinner)
    TextView typeDay;

    @BindView(R.id.textViewStart)
    TextView start;
    @BindView(R.id.textViewContinion)
    TextView continion;
    @BindView(R.id.textViewProgresRezult)
    TextView  progressRezult;
    @BindView(R.id.textViewNext)
    TextView  nextTxt;
    @BindView(R.id.textViewPredict)
    TextView  prediction;

    Button edit, finish;
    boolean isDayCheck[] = {true, true, true, true, true, true, true};
    String[] dayWeekBrif;

    public static DoneHobbyInfoFragment newInstance(int idHobby) {
        Bundle args = new Bundle();
        args.putInt(EXTRA_ID_HOBBY, idHobby);
        DoneHobbyInfoFragment fragment = new DoneHobbyInfoFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        bs = SqliteBD.getInstance(getContext());
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.m_done_hobby_info_fragment, container, false);
        ButterKnife.bind(this,v);
        initView(v);
        return v;

    }

    private void initView(View v) {
        icons = getResources().obtainTypedArray(R.array.icon_res);
        dayWeekBrif = getResources().getStringArray(R.array.day_week_brif);

        Display display = Objects.requireNonNull(getActivity()).getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        for (int i = 0; i < 7; i++) {

            imageDays.get(i).setMinimumWidth(width / 10);
            imageDays.get(i).setMinimumHeight(width / 10);
        }


        edit = (Button) v.findViewById(R.id.buttonEdit);
        finish = (Button) v.findViewById(R.id.buttonFinish);
        edit.setOnClickListener(this);
        finish.setOnClickListener(this);
    }

    private void loadHobbyToDisplay() {
        assert getArguments() != null;
        int id = getArguments().getInt(EXTRA_ID_HOBBY);
        hobby = bs.getHobbyForId(id);
        hobby.setCount(bs.summProgressHobby(id));
        shelder = bs.getShedulerList(id);
        title.setText(hobby.getName());
        icon.setImageResource(icons.getResourceId(hobby.getIconId(), 0));
        if (hobby.getDescription().equals("")) description.append("");
        else description.append(hobby.getDescription());
        if (hobby.getStatus() == 0) {
            long dayMs = 1000 * 3600 * 24;
            long time = (DateLab.now() - hobby.getStart()) / dayMs + 1;
            time = time * (1000 - hobby.getCount()) * dayMs / hobby.getCount();
            if (time <= 0) {
                prediction.setText(getResources().getText(R.string.done));
            } else {
                time += DateLab.now();
                prediction.setText(DateLab.rangeToStr(DateLab.rangeTime(DateLab.now(), time), 5));
            }
            String strNextTxt = shelder.nextDay(this.getContext());
            nextTxt.setText(strNextTxt);
            start.setText(DateLab.parceDate(hobby.getStart(), getResources().getStringArray(R.array.mounth)));
            continion.setText(DateLab.rangeToStr(DateLab.rangeTime(hobby.getStart()), 2));
            progressRezult.setText(hobby.getCount() + "/1000");
        } else {
            nextTxt.setText(getResources().getText(R.string.start_today));
        }
        daysStatus(shelder.getType());
        switch (shelder.getType()) {
            case 0:
                dayCheckAll(true);
                break;
            case 1:
            case 2:
                dayCheck(shelder.getDays(0) - 1);
                break;
            case 3:
                for (int i = 0; i < shelder.size(); i++) dayCheck(shelder.getDays(i));
        }


    }

    private void dayCheck(int index) {
        if (isDayCheck[index]) {
            imageDays.get(index).setVisibility(View.INVISIBLE);
            days.get(index).setTextColor(Color.BLACK);
        } else {
            imageDays.get(index).setVisibility(View.VISIBLE);
            days.get(index).setTextColor(Color.WHITE);
        }
        isDayCheck[index] = !isDayCheck[index];
    }

    private void dayCheckAll(boolean on) {
        for (int i = 0; i < 7; i++) {
            isDayCheck[i] = !on;
            dayCheck(i);
        }
    }

    private void daysStatus(int pos) {
        String[] shelderTypeStr = getResources().getStringArray(R.array.shelder_day);
        String str = shelderTypeStr[pos];
        if (shelder.getType() == 2){
            int dayLast=shelder.getDays(0) - shelder.getDone2();
            dayLast=dayLast<0?0:dayLast;
            str = str + String.format(Objects.requireNonNull(getContext()).getString(R.string.remained), dayLast);}
        typeDay.setText(str);
        imageDays.get(0).setVisibility(View.VISIBLE);
        days.get(0).setVisibility(View.VISIBLE);

        switch (pos) {
            case 0:
                for (int i = 0; i < 7; i++) {
                    days.get(i).setText(dayWeekBrif[i]);
                }
                dayCheckAll(true);
                break;
            case 1:
                imageDays.get(0).setVisibility(View.GONE);
                days.get(0).setVisibility(View.GONE);
                for (int i = 0; i < 7; i++) {
                    days.get(i).setText(String.valueOf(i + 1));
                }
                dayCheckAll(false);
                break;
            case 2:
                for (int i = 0; i < 7; i++) {
                    days.get(i).setText(String.valueOf(i + 1));
                }
                dayCheckAll(false);
                break;
            case 3:
                for (int i = 0; i < 7; i++) {
                    days.get(i).setText(dayWeekBrif[i]);
                }
                dayCheckAll(false);

                break;
        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.buttonEdit:
                startActivity(AddHobbyActivity.newIntent(this.getContext(), hobby.getId()));
                break;
            case R.id.buttonFinish:
                AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(this.getContext()));
                builder.setMessage(getResources().getText(R.string.confirm_finish))
                        .setCancelable(true)
                        .setPositiveButton("Ok",
                                (dialog, id1) -> {
                                    bs.finishHobby(hobby);
                                    Objects.requireNonNull(getActivity()).finish();
                                    dialog.cancel();
                                })
                        .setNegativeButton(getString(R.string.cancel),
                                (dialog, id12) -> dialog.cancel());
                AlertDialog alert = builder.create();
                alert.show();
                break;
//           case R.id.buttonDelete:
//            bs.deleteHobbyFull(hobby.getId());
//            getActivity().finish();
//               break;
        }

    }

    @Override
    public void update() {
        loadHobbyToDisplay();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_done_info, menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_graph) {
            Intent intent = GraphActivity.newIntent(getContext(), hobby.getId());
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        loadHobbyToDisplay();
    }
}
