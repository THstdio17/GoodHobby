package ru.thstdio17.goodhobby.dagger;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.thstdio17.goodhobby.room.RoomHelper;

@Module
public class BdModule {
    Context context;

    public BdModule(Context context) {
        this.context = context;
    }

    @Singleton
    @Provides
    RoomHelper getRoomHelper(){
        return new RoomHelper(context);
    }

}
