package ru.thstdio17.goodhobby.dagger;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {BdModule.class})
public interface AppComponent {
}
